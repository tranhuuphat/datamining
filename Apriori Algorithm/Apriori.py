# coding=utf-8
import itertools

"""YÊU CẦU NGƯỜI DÙNG NHẬP CHỈ SỐ MINSUPPORT VÀ CONFIDENCE (ĐƠN VỊ %)"""

support = float(input("Nhập giá trị minsupport (%): "))
confidence = float(input("Nhập giá trị confidence (%): "))

"""KHAI BÁO & KHỞI TẠO TẬP ỨNG VIÊN 1-PHẦN TỬ"""
C1 = {}  # C viết tắt của Candicate(Ứng viên)
"""TỔNG SỐ GIAO TÁC CỦA CSDL GIAO DỊCH CHỨA TRONG TẬP TIN"""
transactions = 0  # Khai báo biến đếm số giao dịch
D = []  # D viết tắt của Dataset(Tập dữ liệu), Biến sử dụng để lưu danh sách các danh sách mặt hàng của 1 giao dịch
T = []  # T viết tắt của Transactions(Các giao tác), Biến sử dụng để lưu danh sách các mặt hàng của 1 giao dịch
"""NHẬP DỮ LIỆU TỪ FILE & TÌM TẬP ỨNG VIÊN 1-PHẦN TỬ """
with open("DataSetApriori.txt", "r") as f:
    for line in f:  # Duyệt dữ liệu của CSDL GD, biến "line" tại 1 thời điểm chứa thông tin của 1 giao dịch
        T = []
        transactions += 1
        for word in line.split():  # Duyệt dữ liệu của một giao dịch, biến "word" chứa một mặt hàng thuộc 1 giao dịch mà biến "line" đang lưu
            T.append(word)  # Chuyển dữ liệu của 1 giao dịch vào danh sách "T"
            """KIỂM TRA SỰ TỒN TẠI CỦA MẶT HÀNG -->THỐNG KÊ SỐ LƯỢNG XUẤT HIỆN CỦA MẶT HÀNG CHỨA TRONG BIẾN "WORD" """
            if word not in C1.keys():  # Nếu mặt hàng không nằm trong từ điển C1 thì thêm mặt hàng với số lượng 1. (Key="Tên mặt hàng" - Value="1")
                C1[word] = 1
            else:  # Nếu mặt hàng đã được thêm trong từ điển C1 thì tăng số lượng lên 1 đơn vị
                count = C1[word]
                C1[word] = count + 1
        D.append(
            T)  # Đưa dữ liệu 1 giao dịch vào vùng chứa tập dữ liệu của các giao dịch (Mỗi giao dịch  được lưu trữ là 1 ds)
"""XUẤT TẬP DỮ LIỆU SAU KHI ĐỌC TỪ FILE & TẬP ỨNG VIÊN 1-PHẦN TỬ TÌM ĐƯỢC"""
print("===================================================================")
print("-----------------------TẬP DỮ LIỆU GIAO DỊCH----------------------")
print(D)
print("===================================================================")
print("-----------------------TẬP ỨNG VIÊN 1-PHẦN TỬ----------------------")
print(C1)
print("-------------------------------------------------------------------")

"""TÍNH TOÁN TẬP PHỔ BIẾN 1-PHẦN TỬ TỪ TẬP ỨNG VIÊN 1-PHẦN TỬ"""
L1 = []  # Tập phổ biến 1 phần tử
for key in C1:  # Duyệt tập ứng viên
    if (100 * C1[key] / transactions) >= support:  # kiểm tra điều kiện
        list = []
        list.append(key)
        L1.append(list)  # Thêm mặt hàng vào tập phổ biến nếu thỏa mãn điều kiện của tập phổ biến
"""XUẤT TẬP PHỔ BIẾN 1-PHẦN TỬ"""
print("-----------------------TẬP PHỔ BIẾN 1-PHẦN TỬ----------------------")
print(L1)
print("===================================================================")

"""HÀM APRIORI_GEN ĐƯỢC CÀI ĐẶT DỰA TRÊN NGUYÊN LÝ APRIORI
 =>SỬ DỤNG ĐỂ TÍNH TOÁN TẬP ỨNG VIÊN K-PHẦN TỬ (CK), SỬ DỤNG TẬP PHỔ BIẾN (K-1) PHẦN TỬ (LK_1)"""
""" GIẢI THÍCH GIẢI THUẬT
1.Cho 2 vòng lặp for lồng nhau :
    Lần lượt tìm tất cả tổ hợp của 1 danh sách các mặt hàng phổ biến(k-1) phần tử với các danh sách còn lại trong tập phổ biến (k-1) phần tử cho trước
  -> Mục đích : Lấy ra được 1 tổ hợp k mặt hàng khác nhau từ 2 ds (k-1) mặt hàng thuộc tập phổ biến (k-1) phần tử
2. Điều kiện tạo thành 1 tổ hợp : 
    +Là hai ds phổ biến chỉ khác nhau phần tử cuối cùng (phần tử có chỉ số length-1)
    --> Điều kiện này có thể bỏ xót tổ hợp ứng viên nhưng đảm bảo rằng tổ hợp ứng viên đó không là tập phổ biến
    --> Không ảnh hưởng đến độ chính xác, tăng tốc chương trình
       *Chứng minh dựa vào nguyên lý apriori
             Giả sử ta có 3 loại mặt hàng : a,b,c và F3=(a,b,c) là tập phổ biến
             ->Theo Apriori thì điều kiện cần để F3 là tập phổ biến là mọi tập con của F3 cũng phải là tập phổ biến
             ->F2:(a,b),(a,c),(b,c) đều là tập phổ biến 
             ->Thuật giải dựa vào điều kiện cần để tìm tập ứng viên
                 Cụ thể từ tập F2 trên có thể không tìm được ứng viên (a,b,c) trong TH F2 không có (a,c)
             Tuy nhiên nếu thuật giải không tìm được có nghĩa là 1 trong 2 điều kiện (a,b) hoặc (a,c) là tập phổ biến không thỏa mãn
             ==>Tập (a,b,c) chắc chắn không là tập phổ biến
             ==> Không tìm thấy Tập ứng viên (a,b,c) không ảnh hưởng đến độ chính xác của thuật toán.(Điều cần chứng minh)
 
    +Giá trị phần tử cuối cùng của danh sách đầu tiên phải nhỏ hơn giá trị phần tử của danh sách thứ hai (lis1[length-1]<list2[length-1])
  -> Mục đích : Dùng thứ tự để loại bỏ các tổ hợp trùng nhau
3. Sau khi Tạo tổ hợp thì kiểm tra có thỏa mãn nguyên lý  Apriori  không-->loại bỏ ứng viên không tìêm năng (không có khả năng trở thành tập phổ biến)
   +Phát sinh tất cả tổ hợp con k-1 phần tử của ds ứng viên k phần tử vừa tìm được.
   +Kiểm tra xem mọi tổ hợp con k-1 phần tử có thuộc tập phổ biến k-1 phần tử không.
   Nếu thỏa mãn thì ds ứng viên vừa tìm được là ds ứng viên tìềm năng (ứng viên có khả năng là tập phổ biến) """


def apriori_gen(Lk_1, k):
    length = k
    Ck = []
    for list1 in Lk_1:
        for list2 in Lk_1:
            count = 0
            c = []
            if list1 != list2:
                while count < length - 1:
                    if list1[count] != list2[count]:
                        break
                    else:
                        count += 1
                else:
                    if list1[length - 1] < list2[length - 1]:
                        for item in list1:
                            c.append(item)
                        c.append(list2[length - 1])
                        if not has_infrequent_subset(c, Lk_1, k):
                            Ck.append(c)
                            c = []
    return Ck


"""HÀM TÌM VÀ TRẢ VỀ TẬP HỢP CÁC TỔ HỢP CHẬP M CỦA S"""


def findsubsets(S, m):
    return set(itertools.combinations(S, m))


"""Hàm has_infrequent_subset kiểm tra xem ứng viên có thỏa mãn nguyên lý Apriori hay không .Việc kiểm tra dựa vào tập phổ biến (k-1) phần tử"""
"""->Điều kiện phải thỏa mãn : Mọi tập con k phần tử của ứng viên (k+1) phần tử đều thuộc tập phổ biến k phần tử"""


def has_infrequent_subset(c, Lk_1, k):
    list = []
    list = findsubsets(c, k)  # Tìm tất cả tổ hợp k phần tử của danh sách ứng viên k+1 phần tử
    for item in list:  # Duyệt lần lượt các tổ hợp để kiểm tra, Biến "item" sẽ có kiểu tuple
        s = []  # Biến s dùng để chứa dữ liệu của item. ->Chuyển từ kiều tuple sang kiểu list
        for l in item:
            s.append(l)
        s.sort()  # Sắp xếp các phần tử trong tổ hợp
        if s not in Lk_1:  # Kiểm tra sự tồn tại của tổ hợp trong tập phổ biến
            return True  # Không có thì trả về true --> Ngừng kiểm tra --> Tập ứng viên c không là tập phổ biến
    return False  # Sau khi tất cả tổ hợp của ứng viên k phần tử đều thuộc tập phổ biến k-1 phần tử thì trả về false"""
    # --> Ứng viên thỏa mãn Apriori (có khả năng là tập phổ biến)"""


"""frequent_itemsets function to compute all frequent itemsets"""
"""Hàm frequent_itemsets điều khiển việc tính toán và xuất tất cả tập phổ biến từ 2 phần tử trở lên (1 phần tử đã xuất trước đó)"""


def frequent_itemsets():
    k = 2  # Số lượng phần tử của tập phổ biến .
    Lk_1 = []  # Tập phổ biến k-1
    Lk = []  # Tập phổ biến k
    L = []  # Danh sách các tập phổ biến
    count = 0  # Biến đếm tấn suất xuất hiện của một thành phần
    transactions = 0  # Biến đếm số giao dịch
    for item in L1:  # Duyệt tập phổ biến 1-phần tử --> Chuyển vào LK_1 --> LK_1 dùng để tìm LK
        Lk_1.append(item)
    while Lk_1 != []:  # Trong khi tập phổ biến k-1 phần tử khác rổng thì tiếp tục tìm tập phổ biến k phần tử
        Ck = []
        Lk = []
        Ck = apriori_gen(Lk_1, k - 1)  # Tìm tập ứng viên k phần tử dựa vào tập phổ biến k-1 phần tử
        print("-----------------------TẬP ỨNG VIÊN %d-PHẦN TỬ----------------------" % k)
        print("Ck: %s" % Ck)
        print("-------------------------------------------------------------------")
        for c in Ck:  # Duyệt lần lượt các ứng viên k phần tử --> Lưu vào c
            count = 0
            transactions = 0
            s = set(c)  # Chuyển tập ứng viên thành kiểu set (tập hợp) --> Lưu vào s
            for T in D:  # Duyệt tập dữ liệu giao dịch để đếm số lần xuất hiện của tập ứng viên --> Tính support
                transactions += 1
                t = set(T)
                if s.issubset(t) == True:  # Kiểm tra sự xuất hiện của ứng viên trong 1 giao dịch
                    count += 1  # Nếu có thì tăng biến đếm số lần xuất hiện lên 1
            if (
                    100 * count / transactions) >= support:  # So sánh với điều kiện minsupport người dùng nhập, Nếu thỏa mãn thì ứng viên là tập phổ biến
                c.sort()
                Lk.append(c)
        Lk_1 = []  # Đặt lại tập phổ biến k-1
        print("-----------------------TẬP PHỔ BIẾN %d-PHẦN TỬ----------------------" % k)
        print(Lk)
        print("===================================================================")
        for l in Lk:  # Duyệt tập phổ biến k phần tử vừa tìm được --> Tập này sẽ đc dùng để tìm tập phổ biến k+1 phần tử=>Chuyển vào LK_1
            Lk_1.append(l)
        k += 1  # Tăng k lên --> Tiếp tục tìm tập phổ biến k+1
        if Lk != []:  # Nếu Tập phổ biến k phần tử khác rổng thì thêm vào ds các tập phổ biến.
            L.append(Lk)

    return L


"""Hàm generate_association_rules xác định và xuất tất cả luật kết hợp cùng với support và confidence tương ứng."""
"""*Chú thích : Trong bài toán luật kết hợp : X-->Y thì quy ước X ở đây được gọi là thành phần thứ nhất, Y là thành phần thứ hai của mối kết hợp"""


def generate_association_rules():
    r = []  # Tập các thành phần 1
    s = []  # Thành phần 1
    m = []  # Thành phần 2
    length = 0  # Chiều dài hay số phần tử của tập phổ biến
    count = 1  # Số phần tử của thành phần 1
    inc1 = 0  # Số lần xuất hiện của thành phần 1 trong CSDL giao dịch
    inc2 = 0  # Số lần xuất hiện cùng nhau thành phần 1 và thành phần 2 trong CSDL giao dịch
    num = 1  # Số thứ tự luật kết hợp tìm được
    L = frequent_itemsets()  # danh sách của danh sách các tập phổ biến.(từ 1,2,3...,k phần tử...,n phần tử).
    print("-----------------------CÁC LUẬT LIÊN KẾT---------------------------")
    print("RULES \t SUPPORT \t CONFIDENCE")
    print("-------------------------------------------------------------------")
    for list in L:  # Biến list tại 1 thời điểm chứa danh sách của các tập phổ biến x phần tử
        for l in list:  # Duyệt lần lượt các tập phổ biến.
            length = len(l)  # Xác định chiều dài(số lượng) phần tử của 1 tập phổ biến
            count = 1  # Xác định số lượng phần tử của thành phần thứ nhất của mối kết hợp-->Tăng đến lúc không còn tổ hợp luật kết hợp nào
            while count < length:  # Điều kiện này để đảm bảo tồn tại mối kết hợp (số lượng thành phần 1 phải nhỏ hơn tổng số lượng của tập phổ biến)
                s = []  # Thành phần thứ nhất
                r = findsubsets(l, count)  # Danh sách các thành phần thứ nhất (count phần tử)
                count += 1
                for item in r:  # Duyệt lần lượt ds các thành phần thứ nhất
                    inc1 = 0
                    inc2 = 0
                    s = []  # Thành phần thứ nhất
                    m = []  # Thành phần thứ hai
                    for i in item:  # Chuyển thành phần thứ nhất từ kiểu tuple sang kiểu list
                        s.append(i)
                    for T in D:  # Duyệt tập dữ liệu các giao dịch
                        if set(s).issubset(set(T)) == True:  # Đếm số lần xuất hiện của thành phần 1
                            inc1 += 1
                        if set(l).issubset(set(
                                T)) == True:  # Đếm số lần xuất hiện cùng nhau của thành phần 1 và thành phần 2 (tập phổ biến)
                            inc2 += 1
                    if 100 * inc2 / inc1 >= confidence:  # Tính độ gắn kết rồi so sánh-->Nếu thỏa mãn thì xuất luật kết hợp
                        for index in l:  # Duyệt tập kết hợp để tìm thành phần 2
                            if index not in s:
                                m.append(index)  # Thành phần 2
                        print("Rule#  %d : %s ==> %s %d %d" % (num, s, m, 100 * inc2 / len(D),
                                                               100 * inc2 / inc1))  # Xuất luật kết hợp cùng với support và confidence
                        num += 1  # Số thứ tự luật kết hợp tìm được


"""GỌI HÀM TÌM LUẬT KẾT HỢP"""
generate_association_rules()
print("--------------------------------------------------------")
